package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.util.Comparator;
import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		domController.parse();
		List<Yarn> yarns = domController.getNotSortedYarnList();

		// sort (case 1)
		yarns.sort(Comparator.comparing(Yarn::getName));
		
		// save
		String outputXmlFile = "output.dom.xml";
		domController.saveListAsXML(yarns, outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(/*xmlFileName*/);

		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();
		saxParser.parse(xmlFileName, saxController);
		yarns = saxController.getNotSortedYarnList();
		
		// sort  (case 2)
		yarns.sort(Comparator.comparing(Yarn::getSeason));
		
		// save
		outputXmlFile = "output.sax.xml";
		domController.saveListAsXML(yarns, outputXmlFile);
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		staxController.parse();
		yarns = staxController.getNotSortedYarnList();
		
		// sort  (case 3)
		yarns.sort(Comparator.comparing(Yarn::getWeight));
		
		// save
		outputXmlFile = "output.stax.xml";
		domController.saveListAsXML(yarns, outputXmlFile);
	}

}
