package com.epam.rd.java.basic.task8;

public class Yarn {
    private String name;
    private String country;
    private String season;
    private int weight;
    private int length;

    public Yarn(String name, String country, String season, int weight, int length) {
        this.name = name;
        this.country = country;
        this.season = season;
        this.weight = weight;
        this.length = length;
    }

    public Yarn() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
