package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Yarn;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private final String xmlFileName;
	DocumentBuilder builder;
	private List<Yarn> notSortedYarnList;

	public DOMController(String xmlFileName) throws ParserConfigurationException {
		this.xmlFileName = xmlFileName;
		builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
	}

	public void parse() throws IOException, SAXException {
		Document document = builder.parse(new File(xmlFileName));
		document.getDocumentElement().normalize();

		notSortedYarnList = new ArrayList<>();
		Node first = document.getElementsByTagName("yarns").item(0);
		NodeList nodeList = first.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node;
				String name = element.getElementsByTagName("name").item(0).getTextContent();
				String country = element.getElementsByTagName("country").item(0).getTextContent();
				String season = element.getElementsByTagName("season").item(0).getTextContent();
				int weight = Integer.parseInt(element.getElementsByTagName("weight").item(0).getTextContent());
				int length = Integer.parseInt(element.getElementsByTagName("length").item(0).getTextContent());
				notSortedYarnList.add(new Yarn(name, country, season, weight, length));
			}
		}
	}

	public List<Yarn> getNotSortedYarnList() {
		return notSortedYarnList;
	}

	public void saveListAsXML(List<Yarn> sortedYarnList, String fileName) throws TransformerException {
		saveDomToFile(createNewDocument(sortedYarnList), fileName);
	}

	private Document createNewDocument(List<Yarn> yarnList) {
		Document document = builder.newDocument();
		Element root = document.createElement("yarns");
		root.setAttribute("xmlns", "https://lpnu.ua");
		root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		root.setAttribute("xsi:schemaLocation", "https://lpnu.ua input.xsd ");
		document.appendChild(root);
		for (Yarn yarn : yarnList) {
			Element yarnElement = document.createElement("yarn");
			root.appendChild(yarnElement);
			Element name = document.createElement("name");
			name.appendChild(document.createTextNode(yarn.getName()));
			yarnElement.appendChild(name);
			Element country = document.createElement("country");
			country.appendChild(document.createTextNode(yarn.getCountry()));
			yarnElement.appendChild(country);
			Element season = document.createElement("season");
			season.appendChild(document.createTextNode(yarn.getSeason()));
			yarnElement.appendChild(season);
			Element weight = document.createElement("weight");
			weight.appendChild(document.createTextNode(yarn.getWeight() + ""));
			yarnElement.appendChild(weight);
			weight.setAttribute("measure", "g");
			Element length = document.createElement("length");
			length.appendChild(document.createTextNode(yarn.getLength() + ""));
			yarnElement.appendChild(length);
			length.setAttribute("measure", "m");
		}
		return document;
	}

	private void saveDomToFile(Document document, String fileName) throws TransformerException {
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		DOMSource dom = new DOMSource(document);
		StreamResult result = new StreamResult(new File(fileName));
		transformer.transform(dom, result);
	}

}
