package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Yarn;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private final String xmlFileName;
	private List<Yarn> notSortedYarnList;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public void parse() throws FileNotFoundException, XMLStreamException {
		notSortedYarnList = new ArrayList<>();
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(new FileInputStream(xmlFileName));
		int eventType;// = reader.getEventType();
		while (reader.hasNext()) {
			eventType = reader.next();
			if (eventType == XMLEvent.START_ELEMENT) {
				switch (reader.getName().getLocalPart()) {
					case "yarn":
						notSortedYarnList.add(new Yarn());
						break;
					case "name":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS)
							notSortedYarnList.get(notSortedYarnList.size() - 1).setName(reader.getText());
						break;
					case "country":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS)
							notSortedYarnList.get(notSortedYarnList.size() - 1).setCountry(reader.getText());
						break;
					case "season":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS)
							notSortedYarnList.get(notSortedYarnList.size() - 1).setSeason(reader.getText());
						break;
					case "weight":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS)
							notSortedYarnList.get(notSortedYarnList.size() - 1).
									setWeight(Integer.parseInt(reader.getText()));
						break;
					case "length":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS)
							notSortedYarnList.get(notSortedYarnList.size() - 1).
									setLength(Integer.parseInt(reader.getText()));
						break;
				}
			}
		}
	}

	public List<Yarn> getNotSortedYarnList() {
		return notSortedYarnList;
	}
	// PLACE YOUR CODE HERE

}