package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Yarn;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

	//private final String xmlFileName;

	private List<Yarn> notSortedYarnList;
	private StringBuilder elementValue;

	public SAXController(/*String xmlFileName*/) {
		//this.xmlFileName = xmlFileName;
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) {
		switch (qName) {
			case "yarns":
				notSortedYarnList = new ArrayList<>();
				break;
			case "yarn":
				notSortedYarnList.add(new Yarn());
				break;
			case "name":
			case "country":
			case "season":
			case "weight":
			case "length":
				elementValue = new StringBuilder();
				break;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) {
		Yarn latest = notSortedYarnList.get(notSortedYarnList.size() - 1);
		switch (qName) {
			case "name": latest.setName(elementValue.toString()); break;
			case "country": latest.setCountry(elementValue.toString()); break;
			case "season": latest.setSeason(elementValue.toString()); break;
			case "weight": latest.setWeight(Integer.parseInt(elementValue.toString())); break;
			case "length": latest.setLength(Integer.parseInt(elementValue.toString())); break;
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) {
		if (elementValue == null) {
			elementValue = new StringBuilder();
		} else {
			elementValue.append(ch, start, length);
		}
	}

	public List<Yarn> getNotSortedYarnList() {
		return notSortedYarnList;
	}
}